﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyIoC;

namespace IoC_Examples.TinyIoc
{
    public static class Lifetimes
    {
        public static void Run()
        {
            var container = new TinyIoCContainer();

            // By default we register concrete types as 
            // multi-instance, and interfaces as singletons
            container.Register<ServiceA>(); // Multi-instance
            container.Register<ServiceB>(); // Multi-instance
            container.Register<IMyService, ServiceA>(); // Singleton 

            // Fluent API allows us to change that behaviour
            container.Register<ServiceA>().AsSingleton(); // Singleton
            container.Register<IMyService, ServiceA>().AsMultiInstance(); // Multi-instance 
        }
    }
}
