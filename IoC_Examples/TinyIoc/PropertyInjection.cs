﻿using TinyIoC;

namespace IoC_Examples.TinyIoc
{
    public static class PropertyInjection
    {
        class TestClassPropertyDependencies
        {
            public IMyService Property1 { get; set; } // Will be set if we can resolve and isn't already set
            public IMyService Property2 { get; set; } // Will be set if we can resolve and isn't already set
            public int Property3 { get; set; } // Will be ignored
            public string Property4 { get; set; } // Will be ignored

            public ServiceB ConcreteProperty { get; set; } // Will be set if we can resolve and isn't already set

            public IMyService ReadOnlyProperty { get; private set; } // Will be ignored
            public IMyService WriteOnlyProperty { internal get; set; } // Will be ignored (no way to know if it's already set)
        }
        
        public static void Run()
        {
            var container = new TinyIoCContainer();
            var input = new TestClassPropertyDependencies();
            container.BuildUp(input); // Properties are now set
        }
    }
}