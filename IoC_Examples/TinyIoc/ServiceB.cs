﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace IoC_Examples.TinyIoc
{
    public class ServiceB : IMyService
    {
        public void DoOperation() { }
    }
}
