﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TinyIoC;

namespace IoC_Examples.TinyIoc
{
    public static class Resolve
    {
        public static void Run()
        {
            var container = new TinyIoCContainer();

           var instance = container.Resolve<ServiceA>(); // Unnamed
            instance = container.Resolve<ServiceA>("Name"); // Named

            container.Resolve<ServiceA>(
                new ResolveOptions() { UnregisteredResolutionAction = UnregisteredResolutionActions.Fail });
            container.Resolve<ServiceA>("Name",
                new ResolveOptions() { NamedResolutionFailureAction = NamedResolutionFailureActions.AttemptUnnamedResolution });

            container.Resolve<ServiceA>(new NamedParameterOverloads() { { "property1", 12 }, { "property2", "Testing" } });

            container.CanResolve<ServiceA>(
                new ResolveOptions() { UnregisteredResolutionAction = UnregisteredResolutionActions.Fail });
            container.CanResolve<ServiceA>("Name",
                new ResolveOptions() { NamedResolutionFailureAction = NamedResolutionFailureActions.AttemptUnnamedResolution });
            container.CanResolve<ServiceA>(new NamedParameterOverloads() { { "property1", 12 }, { "property2", "Testing" } });

            ServiceA resolvedType;
            var resolved = container.TryResolve<ServiceA>("MyName", out resolvedType); 
            
            container.Resolve<IMyService>("MyType1"); // named registration
            container.Resolve<IMyService>("MyType2"); // 2nd named registration
            IEnumerable<IMyService> myResolvedTypes = container.ResolveAll<IMyService>(); 
        }
    }
}
