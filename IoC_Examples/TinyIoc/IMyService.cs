﻿namespace IoC_Examples.TinyIoc
{
    public interface IMyService
    {
        void DoOperation();
    }
}