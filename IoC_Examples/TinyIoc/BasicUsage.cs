﻿using System.Data;
using TinyIoC;

namespace IoC_Examples.TinyIoc
{
    public static class Registration
    {
        public static void RunTest()
        {
            var container = new TinyIoCContainer();

            //Try to register all types from current assmebly
            container.AutoRegister();

            //Simple registration
            container.Register<ServiceA>();
            container.Register<DependencyA>();
            container.Register<IMyService, ServiceA>();

            //Named registration
            container.Register<ServiceA>("Name"); // Name

            // Specifying a factory delegate
            container.Register<ServiceA>((c, p) => new ServiceA(new DependencyA()));

            // Specifying an already constructed instance
            var instance = new ServiceA(new DependencyA());
            container.Register<IMyService, ServiceA>(instance);

            //By default TinyIoC uses the "greediest" constructor it can satisfy 
            // This example forces this overload:
            // public ServiceA(string property)
            container.Register<ServiceA>().UsingConstructor(() => new ServiceA("my value"));

            container.RegisterMultiple<IMyService>(new[] { typeof(ServiceA), typeof(ServiceB) });
        }
    }
}
