﻿using System;

namespace IoC_Examples.TinyIoc
{
    public class ServiceA : IMyService
    {
        private IDependency _dependency;
        private string _prop1;

        public ServiceA(IDependency dependency, string prop1)
        {
            _dependency = dependency;
            _prop1 = prop1;
        }

        public ServiceA(IDependency dependency)
        {
            _dependency = dependency;
        }

        public ServiceA(Func<IDependency> dependency)
        {
            _dependency = dependency.Invoke();
        }

        public ServiceA(Func<string, IDependency> dependency)
        {
            _dependency = dependency.Invoke("Named");
        }

        public void DoOperation() { }

        public ServiceA(string prop1)
        {
            _prop1 = prop1;
        }
        public ServiceA(string value, string prop1)
        {
            _prop1 = prop1;
        }
    }
}